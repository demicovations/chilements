﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Chilements {
    public static class GetChildrenElements {
        /// <summary>
        /// Gets all UIElement non container type controls (Button, Checkbox...) recursively from conatiner
        /// </summary>
        /// <param name="container">Container</param>
        /// <returns>All UIElements non container type</returns>
        public static IEnumerable<UIElement> FromContainer(Panel container) {

            //list declaration
            List<UIElement> fromContainer = container.Children.Cast<UIElement>().ToList();
            List<UIElement> returnList = new List<UIElement>();


            foreach (UIElement elem in fromContainer.OfType<UIElement>()) {

                var tryPanel = elem as Panel;
                if (tryPanel == null) {  //elem is Classic Control OR Border

                    var tryBorder = elem as Border;
                    if (tryBorder == null) {

                        //elem is non-container
                        returnList.Add(elem);

                    } else {  //elem is border
                        UIElement elFromBorder = GetChildFromBorder(tryBorder);

                        var tryPanelFromBorder = elFromBorder as Panel;
                        if (tryPanelFromBorder == null) {
                            //elFromBorder is non-container
                            returnList.Add(elFromBorder);

                        } else {
                            //elFromBorder is panel
                            returnList.AddRange(FromContainer(tryPanelFromBorder));
                        }
                    }
                } else {
                    //elem is panel
                    returnList.AddRange(FromContainer(tryPanel));
                }
            }
            return returnList;
        }

        /// <summary>
        /// gets all container type elements recursively from border control
        /// </summary>
        /// <param name="b">Border type control</param>
        /// <returns>Container from border</returns>
        private static UIElement GetChildFromBorder(System.Windows.Controls.Border b) {
            if (b.Child.GetType() == typeof(Border)) {
                return GetChildFromBorder(b.Child as Border);
            }
            return b.Child;
        }
    }
}